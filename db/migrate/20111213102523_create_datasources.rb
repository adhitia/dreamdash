class CreateDatasources < ActiveRecord::Migration
  def self.up
    create_table :datasources do |t|
      t.integer :user_id
      t.string :database
      t.string :host
      t.string :database_name
      t.string :user
      t.string :password
      t.integer :database_port

      t.timestamps
    end
  end

  def self.down
    drop_table :datasources
  end
end
