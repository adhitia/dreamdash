class CreateCharts < ActiveRecord::Migration
  def self.up
    create_table :charts do |t|
	  t.string :table_one
	  t.string :table_two
	  t.string :x_axis
	  t.string :y_axis
	  t.integer :user_id
	  t.integer :dashboard_id
      t.timestamps
    end
  end

  def self.down
    drop_table :charts
  end
end
