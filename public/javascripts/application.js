$(document).ready(function(){
  $( "#dsrc li" ).draggable({
    appendTo: "body",
    helper: "clone"
  });
  $( ".droppable" ).droppable({
    activeClass: "ui-state-default",
    hoverClass: "ui-state-hover",
    accept: ":not(.ui-sortable-helper)",
    drop: function( event, ui ) {
      $( this ).find( ".placeholder" ).remove();
      $(this).find(".x_value").val(ui.draggable.text());
      $( "<span></span>" ).text( ui.draggable.text() ).appendTo( this );
    }
  })
});