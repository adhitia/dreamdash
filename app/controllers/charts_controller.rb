class ChartsController < ApplicationController
  layout 'admin'
  before_filter :hide_menu
  before_filter :initialize_chart, :except=>[:index, :create, :new, :test_draggable]
	
  def index
    @charts = Chart.find(:all)
  end
	
  def new
    @chart = current_user.charts.create!
    redirect_to [:edit, @chart]
  end
  
  def create
    @chart = Chart.new(params[:chart])
    respond_to do |format|
      if @chart.save     	
        format.html { redirect_to("/dashboard", :notice => 'Chart was successfully created.') }
        format.xml  { render :xml => @chart, :status => :created, :location => @chart}
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @chart.errors, :status => :unprocessable_entity }
      end
    end
  end

  def edit
  end

  def test_draggable
    
  end

  private

  def hide_menu
    @hide_dashboard_menu=true
  end

  def initialize_chart
    @chart = Chart.find(params[:id])
  end
  
end
