class PagesController < ApplicationController
  # GET /pages
  # GET /pages.xml
  def index
    @pages = Page.all
    @alltables = DB.tables
    if params[:table]
      @datasets = DB.from(params[:table])
      @jan = @datasets.filter{(created_at > 12.months.ago) & (created_at < 11.months.ago)}
      @feb = @datasets.filter{(created_at > 11.months.ago) & (created_at < 10.months.ago)}
      @mar = @datasets.filter{(created_at > 10.months.ago) & (created_at < 9.months.ago)}
      @apr = @datasets.filter{(created_at > 9.months.ago) & (created_at < 8.months.ago)}
      @may = @datasets.filter{(created_at > 8.months.ago) & (created_at < 7.months.ago)}
      @jun = @datasets.filter{(created_at > 7.months.ago) & (created_at < 6.months.ago)}
      @jul = @datasets.filter{(created_at > 6.months.ago) & (created_at < 5.months.ago)}
      @avg = @datasets.filter{(created_at > 5.months.ago) & (created_at < 4.months.ago)}
      @sep = @datasets.filter{(created_at > 4.months.ago) & (created_at < 3.months.ago)}
      @okt = @datasets.filter{(created_at > 3.months.ago) & (created_at < 2.months.ago)}
      @nov = @datasets.filter{(created_at > 2.months.ago) & (created_at < 1.months.ago)}
      @dec = @datasets.filter{(created_at > 1.months.ago) & (created_at < Time.now)}
    else
    end
    render :layout => 'admin'
  end
  def home
  	
  end
  
  def test
  	@alltables = DB.tables
  	@charts = Chart.find(:all, :order => "created_at DESC", :conditions => ["user_id = ?", current_user.id])
  	if @charts != nil
  		@charts.each do |chart|
        f_table1 = chart.table_one
        f_table2 = chart.table_two
        @ds1 = DB.from(f_table1)
        @ds1f = @ds1.all
        @ds2 = DB.from(f_table2)
        @ds2f = @ds2.all
  		end
  	end
  	render :layout => 'admin'		
  end
  
  def layers
  	@alltables = DB.tables
  	@layer1 = DB.from(:orders)
  	render :layout => 'admin'	
  end
  
  def createcharts
  	@alltables = DB.tables
  	if session[:table1]
  		test = session[:table1]
  		@ds = DB.from(test)
  		@ds2 = @ds.all.empty? ? [{}] : @ds.all
  	end
  	render :layout => 'admin'	
  end  
  
  def table1
  	session[:table1] = params[:id]
  	if session[:table1]
  		session[:table2] = params[:id]
  	end
  	redirect_to :back
  end
  
  def xaxis1
  	session[:xaxis1] = params[:id]
  	redirect_to :back
  end
  
  def yaxis1
  	session[:yaxis1] = params[:id]
  	@useric = User.find(current_user.id)
    if current_user.table1 and current_user.table2
  		@useric.update_attributes(:table3 => current_user.table1, :table4 => current_user.table2, :xaxis2 => current_user.xaxis1, :yaxis2 => current_user.yaxis1)
  		@useric.update_attributes(:table1 => session[:table1].to_s, :table2 => session[:table2].to_s, :xaxis1 => session[:xaxis1], :yaxis1 => session[:yaxis1])
    elsif current_user.table1
  		@useric.update_attributes(:table3 => session[:table1].to_s, :table4 => session[:table2].to_s, :xaxis2 => session[:xaxis1], :yaxis2 => session[:yaxis1])
    else
  		@useric.update_attributes(:table1 => session[:table1].to_s, :table2 => session[:table2].to_s, :xaxis1 => session[:xaxis1], :yaxis1 => session[:yaxis1])
    end
  	session[:table1] = nil
  	session[:table2] = nil
  	session[:xaxis1] = nil
  	session[:yaxis1] = nil
  	redirect_to "/dashboard"
  end    

  # GET /pages/1
  # GET /pages/1.xml
  def show
    @page = Page.find_by_permalink(params[:id])
    @documentations = Page.find(:all, :conditions => ["documentation = ? and permalink != ?", true, "documentation"])
    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @page }
    end
  end

  # GET /pages/new
  # GET /pages/new.xml
  def new
    @page = Page.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @page }
    end
  end

  # GET /pages/1/edit
  def edit
    @page = Page.find(params[:id])
  end

  # POST /pages
  # POST /pages.xml
  def create
    @page = Page.new(params[:page])

    respond_to do |format|
      if @page.save
        format.html { redirect_to(@page, :notice => 'Page was successfully created.') }
        format.xml  { render :xml => @page, :status => :created, :location => @page }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @page.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /pages/1
  # PUT /pages/1.xml
  def update
    @page = Page.find(params[:id])

    respond_to do |format|
      if @page.update_attributes(params[:page])
        format.html { redirect_to(@page, :notice => 'Page was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @page.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /pages/1
  # DELETE /pages/1.xml
  def destroy
    @page = Page.find(params[:id])
    @page.destroy

    respond_to do |format|
      format.html { redirect_to(pages_url) }
      format.xml  { head :ok }
    end
  end

end
