class Chart < ActiveRecord::Base
	belongs_to :user
	belongs_to :dashboard

  has_many :layers
  
end
