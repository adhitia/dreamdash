class Dashboard < ActiveRecord::Base
	has_many :charts
	belongs_to :user
end
