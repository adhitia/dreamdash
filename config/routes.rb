Dreamdash::Application.routes.draw do
  resources :dashboards do
  	put :change, :on => :member
  end
  
  resources :charts do
    collection do
      get :test_draggable
    end
  end
  
  resources :datasources
  

  resources :pages do
  	put :table1, :on => :member
  	put :xaxis1, :on => :member
  	put :yaxis1, :on => :member
  end
  
  devise_for :users

  match "dashboard" => "pages#test"
  match "layers" => "pages#layers"  
  match "new_chart" => "pages#createcharts"

  root :to => "pages#home"
end
