RailsAdmin.config do |config|
  config.excluded_models << Chart
  config.excluded_models << Datasource
  config.excluded_models << User
  
  config.model Page do
    edit do
      field :title
      field :meta_description
      field :meta_keywords
      field :permalink      
      field :body, :text do
        ckeditor true
      end
      field :documentation
    end
    list do
    	field :title
    	field :permalink
    	field :documentation
    end
  end  
end